package com.training.basics.wrapperclasses;

public class ExampleStrings {

    char[] chars = { 'H','e','l','l','o','.','.','!','!'};
    String name = " Peter";
    String department = new String("ABCD");
    String duplicate = "abcd";
    String greetings = new String(chars);
    public void printstrings(){
        System.out.println(name);
        System.out.println(department);
        System.out.println(chars);
        System.out.println(greetings);
        System.out.println(greetings.length());
        String greetPeter = greetings.concat(name);
        System.out.println(greetPeter);
        System.out.println(name+" "+ greetings);
        System.out.println(department.charAt(3));
        System.out.println(department.compareTo(duplicate));
        System.out.println(department.compareToIgnoreCase(duplicate));
        System.out.println(greetings.endsWith("$"));

    }

    StringBuffer vehicalName = new StringBuffer("citreon c5");
    StringBuilder city = new StringBuilder("Hyderabad");



}
