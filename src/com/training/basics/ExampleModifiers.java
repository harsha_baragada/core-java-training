package com.training.basics;

public abstract class ExampleModifiers {

    int i = 0; // default
    private String name = "Peter"; // class level access
    public boolean flag = true; // accessible through out the project
    protected double weight = 23.23;
    public final float age = 12.1f;

    // non access modifiers
    public static String sayHello() {
        return "Hello world..!";
    }
    public final int getCount(int i){
        return i++;
    }

    public abstract String printAge();

}
