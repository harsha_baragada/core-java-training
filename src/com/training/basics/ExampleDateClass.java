package com.training.basics;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * y   = year   (yy or yyyy)
 * M   = month  (MM)
 * d   = day in month (dd)
 * h   = hour (0-12)  (hh)
 * H   = hour (0-23)  (HH)
 * m   = minute in hour (mm)
 * s   = seconds (ss)
 * S   = milliseconds (SSS)
 * z   = time zone  text        (e.g. Pacific Standard Time...)
 * Z   = time zone, time offset (e.g. -0800)
 */

public class ExampleDateClass {

    Date today = new Date();

    public void printDate() throws InterruptedException {
        System.out.println(today);
        System.out.println(today.getTime());
        Thread.sleep(1000);
        Date later = new Date();
        System.out.println(later);
        System.out.println(later.getTime());
        System.out.println(today.after(later));
        System.out.println(today.before(later));
        System.out.println(later.toString());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy 'at' HH:mm:ss E zzz");
        System.out.println(simpleDateFormat.format(today));

        GregorianCalendar calendar = new GregorianCalendar(2020, 07, 03);
        System.out.println(calendar);


    }
}
