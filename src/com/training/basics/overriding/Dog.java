package com.training.basics.overriding;

public class Dog extends Animal{


    public void eat(){
        System.out.println("Dog is eating");
    }

    public void bark(){
        System.out.println("dog is barking");
    }
}
