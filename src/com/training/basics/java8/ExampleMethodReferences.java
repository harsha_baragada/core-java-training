package com.training.basics.java8;

import java.util.ArrayList;
import java.util.List;

public class ExampleMethodReferences {

    public static void main(String[] args){
        List<String> fruits = new ArrayList<>();
        fruits.add("Mango");
        fruits.add("apple");
        fruits.add("Banana");
        fruits.add("Orange");
        fruits.add("Water melon");

        fruits.forEach(System.out::println);

    }
}
