package com.training.basics.java8;

import java.util.ArrayList;
import java.util.List;

public class ExampleLambdaExpressions {


    public static void main(String[] args) {
        ExampleLambdaExpressions exampleLambdaExpressions = new ExampleLambdaExpressions();

        MathOperation addition = (int a, int b) -> a + b;
        MathOperation subtraction = (int a, int b) -> a - b;
        MathOperation multiplication = (int a, int b) -> a * b;
        MathOperation division = (int a, int b) -> a / b;

        System.out.println(exampleLambdaExpressions.operate(4, 5, addition));
        System.out.println(exampleLambdaExpressions.operate(4, 5, subtraction));
        System.out.println(exampleLambdaExpressions.operate(4, 5, multiplication));
        System.out.println(exampleLambdaExpressions.operate(4, 5, division));

        List<String> fruits = new ArrayList<>();
        fruits.add("Mango");
        fruits.add("apple");
        fruits.add("Banana");
        fruits.add("Orange");
        fruits.add("Water melon");

        fruits.forEach(fruit -> System.out.println(fruit));


    }

    interface MathOperation {
        int operation(int a, int b);
    }

    interface GreetingService {
        void sayMessage(String message);
    }

    private int operate(int a, int b, MathOperation mathOperation) {
        return mathOperation.operation(a, b);
    }
}


