package com.training.basics;

public class ExampleDataTypes {

    byte x =10;
    short y = 12254;
    int z = 1203365635;
    long a =9999999999L;
    float b = 1.0F;
    double d = 90.99D;
    char c = 'a';
    boolean isSwitchedOn = true;

    public ExampleDataTypes() {
        System.out.println("Hey now you are looking at example data types class..!!");
    }

    public ExampleDataTypes(byte x, short y, int z, long a, float b, double d, char c, boolean isSwitchedOn) {
        System.out.println("This is the parameterized constructor..!");
        this.x = x;
        this.y = y;
        this.z = z;
        this.a = a;
        this.b = b;
        this.d = d;
        this.c = c;
        this.isSwitchedOn = isSwitchedOn;
    }

    public void printDataTypes(){

        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        System.out.println(a);
        System.out.println(b);
        System.out.println(d);
        System.out.println(c);
        System.out.println(isSwitchedOn);
    }

}
