package com.training.basics.collections;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;

public class ExampleCollections {

    public void printCollections() {
        Collection<Integer> ids = new ArrayList<>();
        ids.add(1001);
        ids.add(1234);
        ids.add(1256);
        ids.add(3604);
        System.out.println(ids.size());
        System.out.println(ids.contains(1001));
        System.out.println(ids.contains(10012));
        AbstractCollection<String> abstractCollection = new ArrayList<>();
    }

}
