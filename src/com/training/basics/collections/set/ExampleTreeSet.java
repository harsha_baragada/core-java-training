package com.training.basics.collections.set;

import java.util.*;

public class ExampleTreeSet {

    List<Integer> numbers = new ArrayList<>();


    public void printTreeSet(){

        for (int i =0 ; i <20; i ++){
            numbers.add(i);
            numbers.add(i);
        }
        Collections.shuffle(numbers);
        System.out.println(numbers);
        Collection<Integer> shuffledList = new ArrayList<>(numbers);
        SortedSet<Integer> treeSet = new TreeSet<>(shuffledList);
        System.out.println(treeSet);

    }
}
