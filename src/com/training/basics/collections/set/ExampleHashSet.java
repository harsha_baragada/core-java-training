package com.training.basics.collections.set;

import java.util.HashSet;
import java.util.Set;

public class ExampleHashSet {
    Set<String> countries = new HashSet<>();
    public void printHashSet(){

        countries.add("India");
        countries.add("USA");
        countries.add("Egypt");
        countries.add("Korea");
        countries.add("Italy");
        countries.add("Netherlands");
        countries.add("Brazil");
        countries.add("India");
        System.out.println(countries);
        System.out.println(countries.size());
        countries.remove("egypt");
        System.out.println(countries);
        System.out.println(countries.hashCode());
    }


}
