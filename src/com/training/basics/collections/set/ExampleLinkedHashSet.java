package com.training.basics.collections.set;

import java.util.LinkedHashSet;

public class ExampleLinkedHashSet {

    LinkedHashSet<Double> salaries = new LinkedHashSet<>();
    public void printSalaries(){

        salaries.add(23098.33);
        salaries.add(19033.34);
        salaries.add(30947.33);
        salaries.add(20339.34);
        salaries.add(21345.09);
        salaries.add(93945.22);
        System.out.println(salaries);
    }

}
