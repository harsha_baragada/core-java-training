package com.training.basics.collections.map;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class ExampleLinkedHashMap {

    Map<Integer,String> map = new LinkedHashMap<Integer,String>(15,.75f,true);
    public void printLinkedHashMap(){
        map.put(1001,"Peter");
        map.put(1002,"Bob");
        map.put(1003,"Eva");
        map.put(1004,"Mark");
        map.put(1003,"Jammie");
        map.put(1006,"Emile");
        map.put(1007,"Peter");
        System.out.println(map.get(1006));
        System.out.println(map.get(1004));
        System.out.println(map);

        Set  set = map.entrySet();

        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            Map.Entry me = (Map.Entry)iterator.next();
            System.out.println(me.getKey());
            System.out.println(me.getValue());
        }
    }
}
