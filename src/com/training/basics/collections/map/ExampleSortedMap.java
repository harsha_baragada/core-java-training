package com.training.basics.collections.map;

import java.util.*;

public class ExampleSortedMap {
    SortedMap<Integer, String> map = new TreeMap<>();

    public void printSortedMap() {
        map.put(1002, "Bob");
        map.put(1007, "Peter");
        map.put(1005, "Jammie");
        map.put(1004, "Mark");
        map.put(1001, "Peter");
        map.put(1006, "Emile");
        map.put(1003, "Eva");

        System.out.println(map);
        System.out.println(map.lastKey());
        System.out.println(map.tailMap(1005));
        System.out.println(map.subMap(1003,1005));
    }
}
