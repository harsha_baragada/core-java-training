package com.training.basics.collections.map;

import java.util.HashMap;
import java.util.Map;

public class ExampleMap {

    Map<Integer, String> map = new HashMap<>();
    public void printMap(){

        map.put(1001,"Peter");
        map.put(1002,"Bob");
        map.put(1003,"Eva");
        map.put(1004,"Mark");
        map.put(1003,"Jammie");
        map.put(1006,"Emile");
        map.put(1007,"Peter");

        System.out.println(map);
        System.out.println(map.containsKey(1003));
        System.out.println(map.containsValue("Emile"));
        System.out.println(map.get(1005));
        System.out.println(map.hashCode());
        System.out.println(map.values());
        System.out.println(map.keySet());
        System.out.println(map.remove(1004));
        System.out.println(map);
        System.out.println(map.size());
    }
}
