package com.training.basics.collections.queue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class ExampleQueue {

    public void printQueue(){

        Queue<Integer > queue = new PriorityQueue<>();
        Queue<Integer> list = new LinkedList<>();

        for (int i = 0; i<9; i++){
            queue.add(i);
        }
        System.out.println(queue);
        System.out.println(queue.peek());
        System.out.println(queue);
        System.out.println(queue.remove());
            System.out.println(queue);
        System.out.println(queue.poll());
        System.out.println(queue);
    }
}
