package com.training.basics.collections.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ExampleArrayList {
    List<String> fruits = new ArrayList<>();
    public void printArrayList(){
        fruits.add("Mango");
        fruits.add("Papaya");
        fruits.add("Grapes");
        fruits.add("Strawberry");
        fruits.add("Water melon");
        fruits.add("Apple");
        System.out.println(fruits);
        fruits.add(4,"Guava");
        System.out.println(fruits);
        Collection<String> vegetables = new ArrayList<>();
        vegetables.add("Carrot");
        vegetables.add("Bottle guard");
        vegetables.add("Chillies");
        vegetables.add("Bitter guard");
        fruits.addAll(vegetables);
        System.out.println(fruits);

    }
}
