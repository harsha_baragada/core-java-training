package com.training.basics.collections.list;

import java.util.AbstractSequentialList;
import java.util.LinkedList;
import java.util.List;

public class ExampleLinkedList {

    LinkedList<Integer> list = new LinkedList<>();

    public void printLinkedList(){

        list.add(1001);
        list.add(1002);
        list.add(1992);
        list.add(5028);
        list.add(9823);
        System.out.println(list);
        list.addFirst(5678);
        list.addLast(9367);
        System.out.println(list);
        System.out.println(list.getFirst());
        System.out.println(list.getLast());
    }
}
