package com.training.basics.collections.list;

import java.util.*;

public class ExampleListInterface {

    public void printListInterface(){

        List<String> cities = new ArrayList<>();
        cities.add("Delhi");
        cities.add("Amsterdam");
        cities.add("Newyork");
        cities.add("Cairo");
        cities.add("Seoul");
        cities.add("Melborne");
        System.out.println(cities);
        System.out.println(cities.get(4));
        System.out.println(cities.set(1,"Hyderabad"));
        System.out.println(cities);
        cities.add(1,"Paris");
        System.out.println(cities);
        System.out.println(cities.indexOf("Hyderabad"));
        System.out.println(cities.subList(3,5));

        AbstractList<Integer> list = new ArrayList<>();

        AbstractSequentialList sequentialList = new LinkedList();

    }
}
