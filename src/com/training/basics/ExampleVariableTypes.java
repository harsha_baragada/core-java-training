package com.training.basics;

public class ExampleVariableTypes {

    private String name;

    public static final String DEPARTMENT_NAME="Engineering";  // when declaring a constant we should mention in upper case separated words by underscore

    public ExampleVariableTypes() {
        boolean flag = true;
        System.out.println(flag);
    }

    public  void createLocalVariables(){
        int a = 0; // local variable since it's declared inside a method (or a block or a constructor)
        System.out.println(a);

    }
    public void accessVariables(){
        System.out.println(name);


    }
}
