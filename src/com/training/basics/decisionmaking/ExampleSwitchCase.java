package com.training.basics.decisionmaking;

public class ExampleSwitchCase {

    public void printSwitchCase() {

        char grade = 'Z';

        switch (grade) {

            case 'A':
                System.out.println("Excellent");
                break;
            case 'B':
                System.out.println("Very good");
                break;

            case 'C':
                System.out.println("Good");
                break;

            case 'D':
                System.out.println("Satisfactory");
                break;

            case 'E':
                System.out.println("Poor");
                break;

            case 'F':
                System.out.println("Failed");
                break;

            default:
                System.out.println("grade is not valid");

        }


    }
}
