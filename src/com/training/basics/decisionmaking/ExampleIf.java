package com.training.basics.decisionmaking;

public class ExampleIf {
    // if(boolean_expression){statements to get executed }
    int x = 20;

    public void printIfCondition() {
        if (x > 20) {
            System.out.println(x);
        } else {
            System.out.println("This is else statement");
        }


        if (x % 5 == 0) {
            System.out.println("X is divided by 5");
            if (x % 4 == 0) {
                System.out.println("X is divided by 4");
                if (x % 6 == 0) {
                    System.out.println("x is divided by 6");

                } else{
                    System.out.println("X is not divided by 6");
                }
                if(x% 2 == 0){
                    System.out.println("X is divided by 2");
                }
            }
            else{
                System.out.println("X is not divided by 4");
            }
        }
    }
}


