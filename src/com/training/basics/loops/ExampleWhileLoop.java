package com.training.basics.loops;

public class ExampleWhileLoop {
    //while (Boolean_expression){statement}

    int a = 30;

    public void printWhileLoop() {

        while (a < 60){
            System.out.println(a);
            a++;
        }
    }
}
