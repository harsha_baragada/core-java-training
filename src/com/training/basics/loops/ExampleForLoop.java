package com.training.basics.loops;

import java.util.Arrays;
import java.util.List;

public class ExampleForLoop {

    String [] fruits ={"Apple", "Banana", "Papaya","Pears", "Strawberries", "Oranges"};
    public void printForLoop(){
        for (int i = 0; i < fruits.length ;i ++){
            if(i == 4){
                if(fruits.length == 6){
                    break;
                }
            }
            System.out.println(fruits[i]);
        }

        System.out.println("For each loop");

        for (String fruit : fruits  ) {
            System.out.println(fruit);
        }

        //converted array into list
        List<String> fruitList = Arrays.asList(fruits);


        fruitList.forEach(fruit ->{
            System.out.println(fruit);
        });
    }
}
