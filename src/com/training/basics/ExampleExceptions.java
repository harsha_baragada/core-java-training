package com.training.basics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExampleExceptions {


    //checked or compile time exception
    public void readAFile() throws FileNotFoundException {
        File file = new File("C:\\Users\\VIHCADE\\Downloads\\angular_interview_questions.txt");
        FileReader fileReader = new FileReader(file);
    }

    // unchecked exception or runtime exception
    public void readingAnArray() {

        try {
            int[] ints = {7, 9, 4, 5, 6, 1, 23, 2};
            System.out.println(ints[7]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("something is really fishy ");
            e.printStackTrace();
        }
        finally {
            System.out.println("This is finally block");
        }
    }
}
