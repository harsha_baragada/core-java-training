package com.training.basics.multithreading;

public class ExampleThreadClass {

    static class ThreadDemo extends Thread {
        private Thread thread;
        private String threadName;

        public ThreadDemo(String threadName) {
            this.threadName = threadName;
            System.out.println("creating : " + threadName);
        }

        public void run() {
            try {

                for (int i = 4; i > 0; i--) {
                    System.out.println("Thread : " + threadName + " " + i);
                    Thread.sleep(50);
                }
            } catch (InterruptedException e) {
                System.out.println("Thread " + threadName + " interrupted");
            }
            System.out.println("Thread " + threadName + " exiting");
        }

        public void start() {
            System.out.println("Stating " + threadName);
            if (thread == null) {
                thread = new Thread(this, threadName);
                thread.start();
            }
        }
    }
public static void main(String[] args){
        ThreadDemo T1 = new ThreadDemo("Thread 1");
        T1.start();
    ThreadDemo T2 = new ThreadDemo("Thread 2");
    T2.start();
    ThreadDemo T3 = new ThreadDemo("Thread 3");
    T3.start();
    ThreadDemo T4 = new ThreadDemo("Thread 4");
    T4.start();
    ThreadDemo T5 = new ThreadDemo("Thread 5");
    T5.start();
    ThreadDemo T6 = new ThreadDemo("Thread 6");
    T6.start();
    ThreadDemo T7 = new ThreadDemo("Thread 7");
    T7.start();
    ThreadDemo T8 = new ThreadDemo("Thread 8");
    T8.start();
    ThreadDemo T9 = new ThreadDemo("Thread 9");
    T9.start();

}

}
