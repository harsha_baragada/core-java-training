package com.training.basics.multithreading;

public class ExampleMultiThreading {

    static class RunnableDemo implements Runnable {
        private Thread thread;
        private String threadName;

        public RunnableDemo(String threadName) {
            this.threadName = threadName;
        }

        @Override
        public void run() {

            System.out.println("Thread : " + threadName + " starting");
            try {
                for (int i = 4; i > 0; i--) {
                    System.out.println("Thread " + threadName +" " + i);
                    Thread.sleep(50);
                }
            } catch (InterruptedException e) {
                System.out.println("Thread " + threadName + " interrupted");
            }
            System.out.println("Thread " + threadName + " exiting");
        }

        public void start() {
            System.out.println("Starting " + threadName);
            if (thread == null)
                thread = new Thread(this, threadName);
            thread.start();
        }
    }

    public static void main(String[] args) {
        RunnableDemo R1 = new RunnableDemo("Thread-1");
        R1.start();
        RunnableDemo R2 = new RunnableDemo("Thread-2");
        R2.start();
        RunnableDemo R3 = new RunnableDemo("Thread-3");
        R3.start();
        RunnableDemo R4 = new RunnableDemo("Thread-4");
        R4.start();
        RunnableDemo R5 = new RunnableDemo("Thread-5");
        R5.start();
        RunnableDemo R6 = new RunnableDemo("Thread-6");
        R6.start();
        RunnableDemo R7 = new RunnableDemo("Thread-7");
        R7.start();
    }
}
