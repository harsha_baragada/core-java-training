package com.training.basics.multithreading;


class PrintDemo {

    public void printCount() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("Counter --- " + i);
            }
        } catch (Exception e) {
            System.out.println("Thread is interrupted");
        }
    }
}

class ThreadDemo extends Thread {
    private Thread t;
    private String threadName;
    PrintDemo pd;

    public ThreadDemo(String threadName, PrintDemo pd) {
        this.threadName = threadName;
        this.pd = pd;
    }

    public void run() {
        synchronized (pd){
            pd.printCount();

        }
        System.out.println("Thread " + threadName + " exiting");

    }

    public void start() {
        System.out.println("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}

public class ExampleSynchronization {

    public static void main(String[] args) {
        PrintDemo PD = new PrintDemo();
        ThreadDemo T1 = new ThreadDemo("thread - 1", PD);
        ThreadDemo T2 = new ThreadDemo("thread - 2", PD);
        try {
            T1.start();
            T2.start();

        } catch (Exception e) {
            System.out.println("Interrupted");
        }
    }

}
