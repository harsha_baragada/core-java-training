package com.training.basics.operators;

public class ExampleRelationalOperators {

    public boolean equals(int a, int b) {
        return a == b;
    }

    public boolean notEquals(int a, int b) {
        return a != b;
    }

    public boolean greaterThan(int a, int b) {
        return a > b;
    }

    public boolean lessThan(int a, int b) {
        return a < b;
    }

    public boolean greaterThanEqualTo(int a, int b) {
        return a >= b;
    }

    public boolean lessThanEqualTo(int a, int b) {
        return a <= b;
    }
}
