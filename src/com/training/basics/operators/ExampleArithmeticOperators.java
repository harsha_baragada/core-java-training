package com.training.basics.operators;

public class ExampleArithmeticOperators {

    public int addition(int a, int b) {
        return a + b;
    }

    public int subtraction(int a, int b) {
        return a - b;
    }

    public int multiplication(int a, int b) {
        return a * b;
    }

    public int division(int a, int b) {
        return a / b;
    }

    public int modulus(int a, int b) {
        return a % b;
    }

    public int incremental(int a) {
        return ++a;
    }

    public int decremental(int a) {
        return --a;
    }
}
