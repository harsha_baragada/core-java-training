package com.training.basics.operators;

public class ExampleLogicalOperators {

    public boolean and(boolean a, boolean b){
        return a && b;
    }
    public boolean or(boolean a , boolean b){
        return a || b;
    }

    public boolean not(boolean a){
        return !a;
    }
}
