package com.training.basics.operators;

public class ExampleAssignmentOperators {
    int x = 10;
    int a = 1;


    public void printAssignments() {
        System.out.println(x);
        System.out.println(a += x);
        System.out.println(a -= x);// a = a-x
        System.out.println(a *= x);
        System.out.println(a /= x);
        System.out.println(a %= x);
        System.out.println(a <<= x);
        System.out.println(a >>= x);
        System.out.println(a &= x);
        System.out.println(a ^= x);
        System.out.println(a |= x);

    }

    public void exampleTeranaryOperator() { //variable x = (expression) ? value if true : value if false;
        int a = 21;
        int b = (a > 20) ? 10 : 1;
        System.out.println(b);

    }
}
