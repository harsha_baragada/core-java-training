package com.training.basics.generics;

import java.util.ArrayList;
import java.util.List;

public class ExampleGenericMethod {

    public <E> void arrayIterator(E[] fruits){
        System.out.println(" ");

        for (E fruit :
                fruits) {
            System.out.println(fruit);
        }
    }


    public void getPerson(){

        List<Person> persons = new ArrayList<>();
        Person person = new Person();
        person.setId(1001);
        person.setName("Peter");
        person.setSalary(12000.00);
        persons.add(person);

        Person person2 = new Person();
        person2.setId(1002);
        person2.setName("Bob");
        person2.setSalary(23000.00);
        persons.add(person2);

        Person person3 = new Person();
        person3.setId(1004);
        person3.setName("Peter");
        person3.setSalary(62000.00);
        persons.add(person3);


        Person person4 = new Person();
        person4.setId(1034);
        person4.setName("Peter");
        person4.setSalary(12300.00);
        persons.add(person4);

        arrayIterator(persons.toArray());
    }
}
