package com.training.basics.oops.inheritance;

public class ExampleChild extends ExampleParent {


    public ExampleChild(String name, String department, int id) {
        super(name, department, id);
        System.out.println("Arguement constructor in child class");
    }

    public ExampleChild() {
    }

    public void printHello(){
        System.out.println("This is a child class");
    }

}
