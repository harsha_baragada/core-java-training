package com.training.basics.oops.inheritance;

public class ExampleParent {

    private String name;
    private String department;
    private int id;

    public ExampleParent(String name, String department, int id) {
        this.name = name;
        this.department = department;
        this.id = id;
        System.out.println("Hi " + name + "(" + id + ")" + " in " + department);
    }

    public ExampleParent() {
    }

    public void printHello() {
        System.out.println("This is a parent class");
    }


}
