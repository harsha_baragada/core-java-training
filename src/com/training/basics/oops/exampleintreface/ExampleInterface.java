package com.training.basics.oops.exampleintreface;

public interface ExampleInterface extends ExampleParent1,ExampleParent2 {

    public static final String NAME = "Peter";

     String sayHello();
}
