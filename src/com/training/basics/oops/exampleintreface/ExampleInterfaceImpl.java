package com.training.basics.oops.exampleintreface;

public abstract class ExampleInterfaceImpl implements ExampleInterface{
    public abstract String sayHello();
}
