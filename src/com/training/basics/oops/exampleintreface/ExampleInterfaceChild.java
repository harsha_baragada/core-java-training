package com.training.basics.oops.exampleintreface;

public class ExampleInterfaceChild extends ExampleInterfaceImpl{

    @Override
    public String sayHello() {
        return "Hello world";
    }
}
