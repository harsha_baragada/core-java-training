package com.training;

import com.training.basics.ExampleDatastructure;
import com.training.basics.collections.ExampleCollections;
import com.training.basics.collections.list.ExampleArrayList;
import com.training.basics.collections.list.ExampleLinkedList;
import com.training.basics.collections.list.ExampleListInterface;
import com.training.basics.collections.map.ExampleLinkedHashMap;
import com.training.basics.collections.map.ExampleMap;
import com.training.basics.collections.map.ExampleSortedMap;
import com.training.basics.collections.queue.ExampleQueue;
import com.training.basics.collections.set.ExampleHashSet;
import com.training.basics.collections.set.ExampleLinkedHashSet;
import com.training.basics.collections.set.ExampleSet;
import com.training.basics.collections.set.ExampleTreeSet;
import com.training.basics.generics.ExampleGenericClass;
import com.training.basics.generics.ExampleGenericMethod;
import com.training.basics.generics.Person;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        String[] fruits = {"Strawberry", "Papaya", "orange", "Grapes"};
        Integer[] ids = {1001, 1002, 1003, 1004, 1005, 1006};
        Double[] ages = {13.09, 64.65, 24.77, 76.00};
        ExampleGenericMethod exampleGenericMethod = new ExampleGenericMethod();
        exampleGenericMethod.arrayIterator(fruits);
        exampleGenericMethod.arrayIterator(ids);
        exampleGenericMethod.arrayIterator(ages);
        exampleGenericMethod.getPerson();
        ExampleGenericClass<Integer> integerExampleGenericClass = new ExampleGenericClass<>();
        ExampleGenericClass<String> stringExampleGenericClass = new ExampleGenericClass<>();
        ExampleGenericClass<Person> personExampleGenericClass = new ExampleGenericClass<>();

        integerExampleGenericClass.setT(1001);
        stringExampleGenericClass.setT("Max");
        Person person = new Person();
        person.setId(1001);
        person.setName("Peter");
        person.setSalary(12000.00);

        personExampleGenericClass.setT(person);
        System.out.println(integerExampleGenericClass.getT());
        System.out.println(stringExampleGenericClass.getT());
        System.out.println(personExampleGenericClass.getT());

    }

}
